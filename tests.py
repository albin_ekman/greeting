from solution import greeting


def test_same():
    assert greeting("Daniel", "Daniel") == "Hi, we have the same name!"


def test_different():
    assert greeting("Daniel", "Anders") == "Hello Anders"
